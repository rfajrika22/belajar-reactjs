// // soal 1
// var kataPertama = "saya";
// var kataKedua = "senang";
// var kataKetiga = "belajar";
// var kataKeempat = "javascript";

// // jawaban soal 1
// kataKedua = kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1);
// kataKeempat = kataKeempat.toUpperCase();
// var kataGabungan = kataPertama + " " + kataKedua + " " + kataKetiga + " " + kataKeempat;
// console.log(kataGabungan);

// // soal 2
// var kataPertama = "1";
// var kataKedua = "2";
// var kataKetiga = "4";
// var kataKeempat = "5";

// // jawaban soal 2
// var penjumlahan = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat);
// console.log(penjumlahan);

// // soal 3
// var kalimat = 'wah javascript itu keren sekali';
// kalimat = kalimat.split(' ');
// var kataPertama = kalimat[0];
// // jawaban 3
// var kataKedua = kalimat[1];
// var kataKetiga = kalimat[2];
// var kataKeempat = kalimat[3];
// var kataKelima = kalimat[4];

// console.log('Kata Pertama: ' + kataPertama);
// console.log('Kata Kedua: ' + kataKedua);
// console.log('Kata Ketiga: ' + kataKetiga);
// console.log('Kata Keempat: ' + kataKeempat);
// console.log('Kata Kelima: ' + kataKelima);

// // soal 4
// var nilai;
// // jawaban 4
// nilai = 75;
// if (nilai >= 80) console.log("A");
// else if (nilai >= 70) console.log("B");
// else if (nilai >= 60) console.log("C");
// else if (nilai >= 50) console.log("D");
// else console.log("E");

// soal 5
var tanggal = 22;
var bulan = 7;
var tahun = 2020;

// jawaban 5

switch (bulan) {
    case 1:
        bulan = 'January';
        break;
    case 7:
        bulan = 'July';
        break;
    case 8:
        bulan = 'August';
        break;
    default:
        bulan = 'Salah bulan Bro ... !';
        break;
}

console.log(tanggal + " " + bulan + " " + tahun); 